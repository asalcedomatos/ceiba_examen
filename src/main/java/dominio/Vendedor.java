package dominio;

import dominio.repositorio.RepositorioProducto;
import dominio.repositorio.RepositorioGarantiaExtendida;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import dominio.Producto;
import dominio.excepcion.GarantiaExtendidaException;
import dominio.GarantiaExtendida;

public class Vendedor {

	public static final String EL_PRODUCTO_TIENE_GARANTIA = "El producto ya cuenta con una garantia extendida";
	public static final String PRODUCTO_NO_CUENTA_CON_GARANTIA = "Este producto no cuenta con garant�a extendida";
	public static final String CODIGO_INCORRECTO = "El codigo ingresado esta vac�o o se encuentra nulo";
	public static final String NOMBRE_INCORRECTO = "El nombre ingresado esta vac�o o se encuentra nulo";

	private RepositorioProducto repositorioProducto;
	private RepositorioGarantiaExtendida repositorioGarantia;
	private Producto producto;
	private GarantiaExtendida garantia;

	public Vendedor(RepositorioProducto repositorioProducto, RepositorioGarantiaExtendida repositorioGarantia) {
		this.repositorioProducto = repositorioProducto;
		this.repositorioGarantia = repositorioGarantia;

	}

	public void generarGarantia(String codigo, String nombre) {
		if(codigo==null || codigo =="")
			throw new GarantiaExtendidaException(CODIGO_INCORRECTO);
		
		if (nombre == null || nombre =="")
			throw new GarantiaExtendidaException(NOMBRE_INCORRECTO);
			
		Date solicitudeDate = null;
		Date endDate = null;
		if (repositorioGarantia.obtenerProductoConGarantiaPorCodigo(codigo) == null) {
			producto = repositorioProducto.obtenerPorCodigo(codigo);
			Matcher m = Pattern.compile("[aeiouAEIOU]{1}").matcher(codigo);
			int count = 0;
			while (m.find()) {
				count++;
			}
			if (count == 3)
				throw new GarantiaExtendidaException(PRODUCTO_NO_CUENTA_CON_GARANTIA);

			if (producto != null) {
				double priceWarranty = producto.getPrecio() > 500000 ? producto.getPrecio() * 0.2 : producto.getPrecio() * 0.1;
				int amount = producto.getPrecio() > 500000 ? 200 : 100;
				LocalDateTime today = LocalDateTime.now();
				today.getDayOfWeek();
				int days = 0;
				while (days < amount) {
					if (today.getDayOfWeek() != DayOfWeek.MONDAY && today.getDayOfWeek() != DayOfWeek.SUNDAY) {
						if (solicitudeDate == null) {
							solicitudeDate = Date.from(today.atZone(ZoneId.systemDefault()).toInstant());
						}
						days++;
					}
					if (days == (amount - 1) && today.getDayOfWeek() != DayOfWeek.MONDAY && today.getDayOfWeek() != DayOfWeek.SUNDAY) {
						endDate = Date.from(today.atZone(ZoneId.systemDefault()).toInstant());
						days++;
					}
					today = today.plusDays(1);
				}
				if(priceWarranty > 0  && solicitudeDate!=null && endDate!=null) {
					garantia = new GarantiaExtendida(producto, solicitudeDate, endDate, priceWarranty, nombre);
					repositorioGarantia.agregar(garantia);
				}
			}

		}else{
		throw new GarantiaExtendidaException(EL_PRODUCTO_TIENE_GARANTIA);
		}
	}

	public boolean tieneGarantia(String codigo) {
		return repositorioGarantia.obtenerProductoConGarantiaPorCodigo(codigo)!=null ? true : false;
	}

}
