package dominio.integracion;

import static org.junit.Assert.fail;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dominio.Vendedor;
import dominio.GarantiaExtendida;
import dominio.Producto;
import dominio.excepcion.GarantiaExtendidaException;
import dominio.repositorio.RepositorioProducto;
import dominio.repositorio.RepositorioGarantiaExtendida;
import persistencia.sistema.SistemaDePersistencia;
import testdatabuilder.ProductoTestDataBuilder;

public class VendedorTest {

	private static final String COMPUTADOR_LENOVO = "Computador Lenovo";
	
	private SistemaDePersistencia sistemaPersistencia;
	
	private RepositorioProducto repositorioProducto;
	private RepositorioGarantiaExtendida repositorioGarantia;

	@Before
	public void setUp() {
		
		sistemaPersistencia = new SistemaDePersistencia();
		
		repositorioProducto = sistemaPersistencia.obtenerRepositorioProductos();
		repositorioGarantia = sistemaPersistencia.obtenerRepositorioGarantia();
		
		sistemaPersistencia.iniciar();
	}
	

	@After
	public void tearDown() {
		sistemaPersistencia.terminar();
	}

	@Test
	public void generarGarantiaTest() {

		// arrange
		Producto producto = new ProductoTestDataBuilder().conNombre(COMPUTADOR_LENOVO).build();
		repositorioProducto.agregar(producto);
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);

		// act
		vendedor.generarGarantia(producto.getCodigo(),"Jhon Doe");

		// assert
		Assert.assertTrue(vendedor.tieneGarantia(producto.getCodigo()));
		Assert.assertNotNull(repositorioGarantia.obtenerProductoConGarantiaPorCodigo(producto.getCodigo()));

	}
	
	@Test
	public void garantiaPrecioMenorTest(){
		// arrange
		Producto producto  = new ProductoTestDataBuilder().conPrecio(499999).build();
		GarantiaExtendida warranty;
		ArrayList<Object> comparison = null;
		repositorioProducto.agregar(producto);
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);
						
		//act
		vendedor.generarGarantia(producto.getCodigo(), "Jhon Doe");
		warranty = repositorioGarantia.obtener(producto.getCodigo());
		comparison = daysBetweenDates(warranty.getFechaSolicitudGarantia(),100);
		System.out.println("Fecha Warranty "+warranty.getFechaFinGarantia()+"\nFecha generada "+comparison.get(1));
				
		// assert
		Assert.assertNotNull(warranty);
		Assert.assertEquals(100, comparison.get(0));
		Assert.assertEquals(warranty.getFechaFinGarantia(), comparison.get(1));
	}
	
	@Test
	public void garantiaPrecioMayorTest(){
		// arrange
		Producto producto  = new ProductoTestDataBuilder().conPrecio(600000).build();
		GarantiaExtendida warranty;
		ArrayList<Object> comparison = null;
		repositorioProducto.agregar(producto);
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);
						
		//act
		vendedor.generarGarantia(producto.getCodigo(), "Jhon Doe");
		warranty = repositorioGarantia.obtener(producto.getCodigo());
		comparison = daysBetweenDates(warranty.getFechaSolicitudGarantia(),200);
		System.out.println("Fecha Warranty "+warranty.getFechaFinGarantia()+"\nFecha generada "+comparison.get(1));
				
		// assert
		Assert.assertNotNull(warranty);
		Assert.assertEquals(200, comparison.get(0));
		Assert.assertEquals(warranty.getFechaFinGarantia(), comparison.get(1));
	}
	
	public void productoNoCuentaConGarantiaTest() {
		// arrange
		Producto producto  = new ProductoTestDataBuilder().conCodigo("ASMDTXIU").build();
		repositorioProducto.agregar(producto);
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);
		
		// act
		try {
			vendedor.generarGarantia(producto.getCodigo(), "Jhon Doe");
		} catch (Exception e) {
			// assert
			Assert.assertEquals(Vendedor.PRODUCTO_NO_CUENTA_CON_GARANTIA, e.getMessage());
		}
	}

	@Test
	public void productoYaTieneGarantiaTest() {

		// arrange
		Producto producto = new ProductoTestDataBuilder().conNombre(COMPUTADOR_LENOVO).build();
		
		repositorioProducto.agregar(producto);
		
		Vendedor vendedor = new Vendedor(repositorioProducto, repositorioGarantia);

		// act
		vendedor.generarGarantia(producto.getCodigo(),"Jhon Doe");
		try {
			
			vendedor.generarGarantia(producto.getCodigo(),"Jhon Doe");
			fail();
			
		} catch (GarantiaExtendidaException e) {
			// assert
			Assert.assertEquals(Vendedor.EL_PRODUCTO_TIENE_GARANTIA, e.getMessage());
		}
	}
	
	private ArrayList<Object> daysBetweenDates(Date start,int amount){
		int r_days = 1, days=0;
		ArrayList<Object> response = new ArrayList<>();
		LocalDateTime l_start = LocalDateTime.ofInstant(start.toInstant(), ZoneId.systemDefault());
		Date l_end = null;
		while (days < amount) {
			if (l_start.getDayOfWeek() != DayOfWeek.MONDAY && l_start.getDayOfWeek() != DayOfWeek.SUNDAY) {
				days++;
				r_days++;
			}
			if (days == (amount - 1) && l_start.getDayOfWeek() != DayOfWeek.MONDAY && l_start.getDayOfWeek() != DayOfWeek.SUNDAY) {
				l_end = Date.from(l_start.atZone(ZoneId.systemDefault()).toInstant());
				days++;
			}
			l_start = l_start.plusDays(1);
		}
		response.add(r_days);
		response.add(l_end);
		return response;
	}
}
